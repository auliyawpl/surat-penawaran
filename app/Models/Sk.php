<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sk extends Model
{
  use HasFactory;

  protected $table = 'sk';

  protected $fillable = [
    'id_sk',
    'id_penawaran',
    'deskripsi'
];

protected $hidden = [
    'created_at',
    'update_at'
];

  public function penawaran()
  {
    return $this->belongsTo(Penawaran::class);
  }
}
