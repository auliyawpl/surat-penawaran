<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengantar extends Model
{
    use HasFactory;

    protected $table = 'pengantar';

    protected $fillable = [
        'id_pengantar',
        'id_penawaran',
        'nama_lembaga',
        'alamat_tujuan',
        'isi',
        'tanggal'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function penawaran()
    {
        return $this->belongsTo(Penawaran::class);
    }
}
