<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lampiran extends Model
{
    use HasFactory;

    protected $table = 'lampiran';

    protected $fillable = [
        'id_lampiran',
        'id_penawaran',
        'nama_lampiran',
        'deskripsi'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function penawaran()
    {
        return $this->belongsTo(Penawaran::class);
    }
}
