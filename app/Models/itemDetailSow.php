<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class itemDetailSow extends Model
{

    use HasFactory;

    protected $table = 'item_detail_sow';

    protected $fillable = [
        'id_item_detail_sow',
        'id_detail_sow',
        'rincian_pekerjaan',
        'tugas_penawar',
        'tugas_penerima'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function detail_sow()
    {
        return $this->belongsTo(detailSow::class);
    }
}
