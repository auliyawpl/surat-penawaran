<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tujuanPenawaran extends Model
{   
    use HasFactory;

    protected $table = 'tujuan_penawaran';

    protected $fillable = [
        'id_tujuan',
        'nama_perusahaan',
        'alamat',
        'email',
        'no_telepon',
        'pic'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function tujuan_penawaran()
    {
        return $this->belongsToMany(Penawaran::class);
    }
}
