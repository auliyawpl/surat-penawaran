<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sow extends Model
{
    use HasFactory;

    protected $table = 'sow';

    protected $fillable = [
        'id_sow',
        'id_penawaran',
        'nama_sow'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function detail_sow()
    {
        return $this->hasMany(detailSow::class);
    }

    public function penawaran()
    {
        return $this->belongsTo(Penawaran::class);
    }
}
