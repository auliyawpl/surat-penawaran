<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kopSurat extends Model
{
    use HasFactory;

    protected $table = 'kop_surat';

    protected $fillable = [
        'id_kop_surat',
        'id_penawaran',
        'nomor_surat',
        'lampiran',
        'perihal'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function penawaran()
    {
        return $this->belongsTo(Penawaran::class);
    }
}
