<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detailPenawaran extends Model
{
    use HasFactory;

    protected $table = 'detail_penawaran';

        protected $fillable = [
        'id_detail_penawaran',
        'id_penawaran',
        'deskripsi',
        'jumlah',
        'jenis_pekerjaan',
        'biaya',
        'Qty',
        'satuan',
        'harga_satuan',
        'total'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function penawaran()
    {
        return $this->belongsTo(Penawaran::class);
    }
}
