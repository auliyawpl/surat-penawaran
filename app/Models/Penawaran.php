<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penawaran extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_penawaram',
        'tgl_penawaran',
        'id_perusahaan_tujuan',
        'lampiran',
        'alamat',
        'deskripsi',
        'jenis_penawaran'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function tujuan_penawaran()
    {
        return $this->belongsToMany(tujuanPenawaran::class);
    }

    public function detail_penawaran()
    {
        return $this->hasMany(detailPenawaran::class);
    }

    public function lampiran()
    {
        return $this->hasMany(Lampiran::class);
    }

    public function sow()
    {
        return $this->hasMany(Sow::class);
    }

    public function sk()
    {
        return $this->hasMany(Sk::class);
    }

    public function pengantar()
    {
        return $this->hasMany(Pengantar::class);
    }

    public function kop_surat()
    {
        return $this->hasMany(kopSurat::class);
    }
}
