<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detailSow extends Model
{
  
    use HasFactory;

    protected $table = 'detail_sow';

    protected $fillable = [
        'id_detail_sow',
        'id_sow',
        'rincian_pekerjaan',
        'nama_perusahaan',
        'pic_penerima'
    ];

    protected $hidden = [
        'created_at',
        'update_at'
    ];

    public function item_detail_sow()
    {
        return $this->hasMany(itemDetailSow::class);
    }

    public function sow()
    {
        return $this->belongsTo(Sow::class);
    }

}
