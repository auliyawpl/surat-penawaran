<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pengguna extends Model
{
    protected $table = 'pengguna'; 

    protected $fillable = [
        'id_user',
        'nama_lengkap',
        'username',
        'password'

    ];
    
    use HasFactory;

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
